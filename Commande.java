import java.time.LocalDate;
import java.util.Date;

public class Commande {

	public Commande(int id, int quantite, String etat, int id_produit) {
		this.quantite = quantite;
		this.etat = etat;
		this.id = id;
		this.id_produit = id_produit;
		date_creation = LocalDate.now();
		
	}

	private int id;
	private int id_produit;
	private int quantite;
	private String etat;
	private LocalDate date_creation; 
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId_produit() {
		return id_produit;
	}
	public void setId_produit(int id_produit) {
		this.id_produit = id_produit;
	}
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	public String getEtat() {
		return etat;
	}
	public void setEtat(String etat) {
		this.etat = etat;
	}
	public LocalDate getDate_creation() {
		return date_creation;
	}
	public void setDate_creation(LocalDate date_creation) {
		this.date_creation = date_creation;
	}
	@Override
	public String toString() {
		return "Commande [id=" + id + ", id_produit=" + id_produit + ", quantite=" + quantite + ", etat=" + etat
				+ ", date_creation=" + date_creation + "]";
	}
	
	
	


	

}
