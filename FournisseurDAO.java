import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'acc�s aux donn�es contenues dans la table article
 * @version 1.1
 * */
public class FournisseurDAO {

	/**
	 * Param�tres de connexion � la base de donn�es oracle
	 * URL, LOGIN et PASS sont des constantes
	 */
	final static String URL = "jdbc:mysql://localhost:3306/vaccination";
	final static String LOGIN="root";
	final static String PASS="";


	/**
	 * Constructeur de la classe
	 * 
	 */
	public FournisseurDAO()
	{
		// chargement du pilote de bases de donn�es
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e2) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}
	

	/**
	 * Permet d'ajouter un article dans la table article
	 * la r�f�rence de l'article est produite automatiquement par la base de donn�es en utilisant une s�quence
	 * Le mode est auto-commit par d�faut : chaque insertion est valid�e
	 * @param nouvArticle l'article � ajouter
	 * @return le nombre de ligne ajout�es dans la table
	 */
	public int ajouter(Fournisseur nouvFournisseur)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;

		//connexion � la base de donn�es
		try {

			//tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			//pr�paration de l'instruction SQL, chaque ? repr�sente une valeur � communiquer dans l'insertion
			//les getters permettent de r�cup�rer les valeurs des attributs souhait�s de nouvArticle
			ps = con.prepareStatement("INSERT INTO fournisseur (nom, adresse, telephone) VALUES (?, ?, ?)");
			ps.setString(1,nouvFournisseur.getNom());
			ps.setString(2,nouvFournisseur.getAdresse());
			ps.setInt(3,nouvFournisseur.getTelephone());

			//Ex�cution de la requ�te
			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du preparedStatement et de la connexion
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}

	/**
	 * Permet de r�cup�rer un article � partir de sa r�f�rence
	 * @param reference la r�f�rence de l'article � r�cup�rer
	 * @return l'article
	 * @return null si aucun article ne correspond � cette r�f�rence
	 */
	public Fournisseur getFournisseur(int reference)
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		Fournisseur retour=null;

		//connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM fournisseur WHERE id = ?");
			ps.setInt(1,reference);

			//on ex�cute la requ�te
			//rs contient un pointeur situ� jusute avant la premi�re ligne retourn�e
			rs=ps.executeQuery();
			//passe � la premi�re (et unique) ligne retourn�e 
			if(rs.next())
				retour=new Fournisseur(rs.getInt("id"),rs.getString("nom"),rs.getString("adresse"),rs.getInt("telephone"));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du ResultSet, du PreparedStatement et de la Connection
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}

	/**
	 * Permet de r�cup�rer tous les articles stock�s dans la table article
	 * @return une ArrayList d'Articles
	 */
	public List<Fournisseur> getListeFournisseurs()
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		List<Fournisseur> retour=new ArrayList<Fournisseur>();

		//connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM fournisseur");

			//on ex�cute la requ�te
			rs=ps.executeQuery();
			//on parcourt les lignes du r�sultat
			while(rs.next())
				retour.add(new Fournisseur(rs.getInt("id"),rs.getString("nom"),rs.getString("adresse"),rs.getInt("telephone")));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du rs, du preparedStatement et de la connexion
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}

	//main permettant de tester la classe
	public static void main(String[] args)  throws SQLException {

		FournisseurDAO fournisseurDAO=new FournisseurDAO();
		//test de la m�thode ajouter
		Fournisseur a = new Fournisseur(2,"Set de 2 raquettes de ping-pong", "149.9" ,10);
		int retour=fournisseurDAO.ajouter(a);

		System.out.println(retour+ " lignes ajout�es");

		//test de la m�thode getArticle
		Fournisseur a2 = fournisseurDAO.getFournisseur(1);
		System.out.println(a2);

		 //test de la m�thode getListeArticles
		List<Fournisseur> liste=fournisseurDAO.getListeFournisseurs();
		//System.out.println(liste);
		for(Fournisseur art : liste)
		{
			System.out.println(art.toString());
		}

	}
}
