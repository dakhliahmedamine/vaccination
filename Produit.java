import java.time.LocalDate;
import java.util.Date;

public class Produit {

	
	public Produit(int id, String nom, int quantite, Date date_creation, Fournisseur fournisseur, double prixUnitaire) {
		super();
		this.id = id;
		this.nom = nom;
		this.quantite = quantite;
		this.fournisseur = fournisseur;
		this.prixUnitaire =prixUnitaire;
	}
	
	
	private int id;
	private String nom;
	private int quantite;
	private double prixUnitaire;
	private Fournisseur fournisseur;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	public Fournisseur getFournisseur() {
		return fournisseur;
	}
	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}
	
	
	
	public double getPrixUnitaire() {
		return prixUnitaire;
	}
	public void setPrixUnitaire(double prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}
	@Override
	public String toString() {
		return "Produit [id=" + id + ", nom=" + nom + ", quantite=" + quantite + ", date_creation="
				+ ", fournisseur=" + fournisseur + "]";
	}
	
	
	





}
