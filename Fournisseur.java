
public class Fournisseur {
	
	
	public Fournisseur(int id, String nom, String adresse, int telephone) {
		super();
		this.id = id;
		this.nom = nom;
		this.adresse = adresse;
		this.telephone = telephone;
	}

	public Fournisseur(String nom, String adresse, int telephone) {
		super();
		this.nom = nom;
		this.adresse = adresse;
		this.telephone = telephone;
	}
	
	/**
	 * l'identifiant du fournisseur
	 */
	private int id;
	
	/**
	 * le nom du fournisseur
	 */
	private String nom;
	
	/**
	 * l'adresse du fournisseur
	 */
	private String adresse;
	
	/**
	 * le numero telephone du fournisseur
	 */
	private int telephone;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public int getTelephone() {
		return telephone;
	}

	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}

	@Override
	public String toString() {
		return "fournisseur [id=" + id + ", nom=" + nom + ", adresse=" + adresse + ", telephone=" + telephone + "]";
	}
	
	
	
	
	


}
